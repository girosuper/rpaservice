package com.example.rpaService;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RpaServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(RpaServiceApplication.class, args);
	}

}
