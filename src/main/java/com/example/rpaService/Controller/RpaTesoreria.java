/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.rpaService.Controller;

import com.example.rpaService.Exception.FTPErrors;
import com.example.rpaService.Service.FTPService;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 *
 * @author Andres
 */

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/rpa/tesoreria")
public class RpaTesoreria {
    
     @Autowired
    private FTPService ftpService;
    
    //Save the uploaded file to this folder
    private static String UPLOADED_FOLDER = "D://temp//";
    
        @GetMapping("/teso")
        public String indexTeso() { 
            return "upload";
        }
    
    
    @PostMapping("/uploadTeso") // //new annotation since 4.3
    public Map singleFileUploadTeso(@RequestParam("file") MultipartFile file,
                                   RedirectAttributes redirectAttributes) {

        HashMap map = new HashMap<>();
        if (file.isEmpty()) {
                map.put("success", false);
                map.put("key", file.getName());
                map.put("link", "NONE");
            redirectAttributes.addFlashAttribute("message", "Please select a file to upload");
            return map;
        }

        try {

            // Get the file and save it somewhere
            byte[] bytes = file.getBytes();
            Path path = Paths.get(UPLOADED_FOLDER + file.getOriginalFilename());
            Files.write(path, bytes);
            
            
            

            redirectAttributes.addFlashAttribute("message",
                    "You successfully uploaded '" + file.getOriginalFilename() + "'");

        } catch (IOException e) {
            e.printStackTrace();
        }
        
                try {

              ftpService.connectToFTP("172.23.30.36","CSA","3tr0p0s*");
              ftpService.uploadFileToFTP(new File(UPLOADED_FOLDER+file.getOriginalFilename()),"/csa/Traslados/Insumos/CSA/Excel/",""+file.getOriginalFilename());
              //ftpService.downloadFileFromFTP("uploads/foto.png","/home/kaka.png");
              
              ftpService.disconnectFTP();
              
                
                map.put("success", true);
                map.put("key", file.getName());
                map.put("link", UPLOADED_FOLDER);

        } catch (FTPErrors ftpErrors) {
            System.out.println(ftpErrors.getMessage());
        }
    

        return map;
    }

    @GetMapping("/uploadStatusTeso")
    public String uploadStatusTeso() {
        return "uploadStatus";
    }
    
}
